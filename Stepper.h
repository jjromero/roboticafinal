#ifndef STEPPER_H
#define STEPPER_H

#include <Arduino.h>
#include <math.h>
#include "Helpers.h"

struct Step
{
  int coil1;
  int coil2;
  Step *next;
  Step *previous;
};

class Stepper
{
private:
  Step *currentStepCoil;

  int coilA;
  int coilB;
  int coilC;
  int coilD;
  int duration;
  float currentAngle;
  float anglePerStep;
  unsigned long nextUpdate;

  bool moving;
  int steps;
  int currentDelay;
  int currentStep;
  bool forward;

public:
  Stepper(int coilA, int coilB, int coilC, int coilD, int duration);
  void setAngle(float angle);
  void stepForward();
  void stepBackward();
  void move();
  void update();
  void begin();
  bool isMoving();
  void setDuration(int duration);
  float prepareAngle(float angle);
};

#endif
