#ifndef POSITIONER_H
#define POSITIONER_H

#include <math.h>

struct Angles
{
  float a;
  float b;
  float c;
};

class Positioner
{
private:
  float l1;
  float l2;
  float baseHeight;

public:
  Positioner(float l1, float l2);
  Angles getInverseAngles(float x, float y, float z);
  float radToDeg(float rad);
};

#endif
