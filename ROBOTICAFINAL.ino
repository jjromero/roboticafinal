#include "Positioner.h"
#include "Joystick.h"
#include "Arm.h"

struct Position
{
  float X;
  float Y;
  float Z;
};

float CM_PER_UPDATE = 1;
float POSITION_SPEED = CM_PER_UPDATE / (float)512;
float JOYSTICK_THRESHOLD = 50;

int coilA = 4;
int coilB = 5;
int coilC = 6;
int coilD = 7;
int servoBottom = 15;
int servoTop = 14;
int servoTool = 13;
int movementDuration = 200;
int updateDelay = 210;

Arm arm = Arm(servoBottom, servoTop, servoTool, coilA, coilB, coilC, coilD, movementDuration);
Positioner positioner = Positioner(22.5, 13.4);
Joystick joystickLeft = Joystick(A0, A1);
Joystick joystickRight = Joystick(A2, A3);

Position currentPosition, tempPosition;
unsigned long nextUpdate;

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  currentPosition.X = 20;
  currentPosition.Y = 0;
  currentPosition.Z = 10;

  Angles angles = positioner.getInverseAngles(currentPosition.X, currentPosition.Y, currentPosition.Z);
  arm.begin(angles);
  nextUpdate = 0;
}

void loop()
{
  arm.update();

  if (nextUpdate <= millis())
  {
    nextUpdate = millis() + updateDelay;
    JoystickData left = joystickLeft.read();
    JoystickData right = joystickRight.read();
    tempPosition.X = moveAxis(currentPosition.X, left.X);
    tempPosition.Y = moveAxis(currentPosition.Y, left.Y);
    tempPosition.Z = moveAxis(currentPosition.Z, right.X);

    Angles angles = positioner.getInverseAngles(tempPosition.X, tempPosition.Y, tempPosition.Z);
    if (areValidAngles(angles))
    {
      digitalWrite(LED_BUILTIN, LOW);
      currentPosition = tempPosition;
      arm.setAngles(angles);
    }
    else
    {
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }
}

float moveAxis(float current, float increment)
{
  if (abs(increment) > JOYSTICK_THRESHOLD)
  {
    return current + increment * POSITION_SPEED;
  }
  return current;
}

bool areValidAngles(Angles angles)
{
  return !isnan(angles.a) && !isnan(angles.b) && !isnan(angles.c);
}
