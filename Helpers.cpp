#include "Helpers.h"

float rpmToRps(int rpm)
{
  return (float)rpm / (float)60;
}