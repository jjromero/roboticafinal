#ifndef JOYSTICK_H
#define JOYSTICK_H

#include <Arduino.h>

struct JoystickData
{
  int X;
  int Y;
};

class Joystick
{
private:
  int PIN_X;
  int PIN_Y;

public:
  Joystick(int PIN_X, int PIN_Y);
  JoystickData read();
};

#endif
