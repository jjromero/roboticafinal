#ifndef ARM_H
#define ARM_H

#include "Stepper.h"
#include "Servos.h"
#include "Positioner.h"

class Arm
{
private:
  Servos servos;
  Stepper stepper;

public:
  Arm(int servoBottom, int servoTop, int servoTool, int coilA, int coilB, int coilC, int coilD, int duration);
  void setAngles(Angles angles);
  void begin(Angles angles);
  void update();
  bool isMoving();
  void setDuration(int duration);
};

#endif
