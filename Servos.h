#ifndef SERVOS_H
#define SERVOS_H

#include <Adafruit_PWMServoDriver.h>
#include <Arduino.h>
#include <math.h>

class Servos
{
private:
  Adafruit_PWMServoDriver driver;
  int pos0;
  int pos180;

  int servoBottom;
  int servoTop;
  int servoTool;

  float currentAngleBottom;
  float currentAngleTop;
  float currentAngleTool;

  float anglePerStepBottom;
  float anglePerStepTop;
  float anglePerStepTool;

  unsigned long nextUpdate;
  bool moving;

  int _delay;
  int steps;
  int currentStep;

public:
  Servos(int servoBottom, int servoTop, int servoTool, int duration, int steps = 5);
  void setAngles(float bottom, float top, float tool = -1);
  void setServo(uint8_t nServo, int angulo);
  void move();
  void update();
  void begin();
  bool isMoving();
  void setDuration(int duration);
};

#endif
