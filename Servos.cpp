#include "Servos.h"

Servos::Servos(int servoBottom, int servoTop, int servoTool, int duration, int steps)
{
  driver = Adafruit_PWMServoDriver(0x40);
  pos0 = 172;
  pos180 = 650;

  this->steps = steps;
  this->setDuration(duration);

  this->servoBottom = servoBottom;
  this->servoTop = servoTop;
  this->servoTool = servoTool;

  anglePerStepBottom = 0;
  anglePerStepTop = 0;
  anglePerStepTool = 0;

  nextUpdate = 0;
  moving = false;
}

void Servos::setAngles(float bottom, float top, float tool)
{
  // Serial.println("Servos::setAngles");
  float targetTool =
      (tool == -1)
          ? 90 + currentAngleBottom - currentAngleTop
          : tool;

  anglePerStepBottom = (bottom - currentAngleBottom) / (float)steps;
  anglePerStepTop = (top - currentAngleTop) / (float)steps;
  anglePerStepTool = (targetTool - currentAngleTool) / (float)steps;

  currentStep = 1;
  nextUpdate = millis() + _delay;
  moving = true;
}

void Servos::setServo(uint8_t nServo, int angle)
{
  int duty = map(angle, 0, 180, pos0, pos180);
  driver.setPWM(nServo, 0, duty);
}

void Servos::move()
{
  if (currentStep <= steps)
  {
    currentAngleBottom += anglePerStepBottom;
    currentAngleTop += anglePerStepTop;
    currentAngleTool += anglePerStepTool;

    setServo(servoBottom, currentAngleBottom);
    setServo(servoTop, currentAngleTop);
    setServo(servoTool, currentAngleTool);

    // Serial.print("Bottom: ");
    // Serial.println(currentAngleBottom);
    // Serial.print("Top: ");
    // Serial.println(currentAngleTop);
    // Serial.print("Tool: ");
    // Serial.println(currentAngleTool);

    currentStep++;
  }
  else
  {
    // Serial.println("-------------------- Listo Servos :)");
    moving = false;
  }
}

void Servos::update()
{
  unsigned long currentMillis = millis();
  if (moving && nextUpdate <= currentMillis)
  {
    move();
    nextUpdate = currentMillis + _delay;
  }
}

void Servos::begin(float bottom, float top, float tool)
{
  driver.begin();
  driver.setPWMFreq(60);

  currentAngleBottom = bottom;
  currentAngleTop = top;
  currentAngleTool = tool;
}

bool Servos::isMoving()
{
  return moving;
}

void Servos::setDuration(int duration)
{
  _delay = duration / steps;
}
