#include "Positioner.h"
#include <Arduino.h>

Positioner::Positioner(float l1, float l2)
{
  this->l1 = l1;
  this->l2 = l2;
  // todo está sobre una base con altura baseHeight
  this->baseHeight = 6.5;
}

float Positioner::radToDeg(float rad)
{
  return (rad / M_PI) * 180;
}

Angles Positioner::getInverseAngles(float x, float y, float z)
{
  z = z - this->baseHeight;

  Angles result;
  float R = sqrt(x * x + y * y);

  float a = this->l1;
  float b = this->l2;
  float c = sqrt(pow(z, 2) + pow(R, 2));
  float D = (pow(c, 2) - pow(a, 2) - pow(b, 2)) / (2 * a * b);
  result.a = atan2(x, y);
  result.c = atan2(sqrt(1 - D * D), D);
  result.b = atan2(z, R) + atan2(b * sin(result.c), a + b * cos(result.c));

  result.a = this->radToDeg(result.a);
  result.b = this->radToDeg(result.b);
  result.c = this->radToDeg(result.c);

  return result;
}
