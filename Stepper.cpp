#include "Stepper.h"

Stepper::Stepper(int coilA, int coilB, int coilC, int coilD, int duration)
{
  this->coilA = coilA;
  this->coilB = coilB;
  this->coilC = coilC;
  this->coilD = coilD;
  this->setDuration(duration);

  int stepsPerRevolution = 2048;
  anglePerStep = (float)360 / (float)stepsPerRevolution;
  currentAngle = 90;
  // currentAngle = 0;
  nextUpdate = 0;
  moving = false;

  Step *step1, *step2, *step3, *step4;
  step1 = new Step;
  step2 = new Step;
  step3 = new Step;
  step4 = new Step;

  step1->coil1 = this->coilA;
  step1->coil2 = this->coilB;
  step1->previous = step4;
  step1->next = step2;

  step2->coil1 = this->coilB;
  step2->coil2 = this->coilC;
  step2->previous = step1;
  step2->next = step3;

  step3->coil1 = this->coilC;
  step3->coil2 = this->coilD;
  step3->previous = step2;
  step3->next = step4;

  step4->coil1 = this->coilD;
  step4->coil2 = this->coilA;
  step4->previous = step3;
  step4->next = step1;

  currentStepCoil = step1;
}

void Stepper::stepForward()
{
  digitalWrite(currentStepCoil->coil1, LOW);
  digitalWrite(currentStepCoil->coil2, LOW);
  currentStepCoil = currentStepCoil->next;
  digitalWrite(currentStepCoil->coil1, HIGH);
  digitalWrite(currentStepCoil->coil2, HIGH);
  currentAngle += anglePerStep;
}

void Stepper::stepBackward()
{
  digitalWrite(currentStepCoil->coil1, LOW);
  digitalWrite(currentStepCoil->coil2, LOW);
  currentStepCoil = currentStepCoil->previous;
  digitalWrite(currentStepCoil->coil1, HIGH);
  digitalWrite(currentStepCoil->coil2, HIGH);
  currentAngle -= anglePerStep;
}

float Stepper::prepareAngle(float angle)
{
  return angle;
  // angle = 0 - 360
  // angle + 180 = 180 - 540

  if (angle + 180 >= 360)
  {
    // 0-180 = 0-180
    return angle + 180 - 360;
  }
  else
  {
    // return = 180-360
    return angle + 180;
  }
}

void Stepper::setAngle(float angle)
{
  steps = abs((prepareAngle(angle) - currentAngle) / anglePerStep);
  currentDelay = duration / steps;

  forward = currentAngle < angle;
  currentStep = 1;
  nextUpdate = millis() + currentDelay;
  moving = true;

  // Serial.print("Angle: ");
  // Serial.println(prepareAngle(angle));
  // Serial.print("Steps: ");
  // Serial.println(steps);
  // Serial.print("CurrentDelay: ");
  // Serial.println(currentDelay);
  // Serial.print("CurrentAngle: ");
  // Serial.println(currentAngle);
  // Serial.println("-------------");
}

void Stepper::move()
{
  if (currentStep <= steps)
  {
    currentStep++;
    if (forward)
    {
      stepForward();
    }
    else
    {
      stepBackward();
    }
    // Serial.print("Stepper: ");
    // Serial.println(currentAngle);
  }
  else
  {
    // Serial.println("-------------------- Listo Stepper :)");
    moving = false;
  }
}

void Stepper::update()
{
  unsigned long currentMillis = millis();
  if (moving && nextUpdate <= currentMillis)
  {
    move();
    nextUpdate = currentMillis + currentDelay;
  }
}

void Stepper::begin()
{
  pinMode(coilA, OUTPUT);
  pinMode(coilB, OUTPUT);
  pinMode(coilC, OUTPUT);
  pinMode(coilD, OUTPUT);

  digitalWrite(currentStepCoil->coil1, HIGH);
  digitalWrite(currentStepCoil->coil2, HIGH);
}

bool Stepper::isMoving()
{
  return moving;
}

void Stepper::setDuration(int duration)
{
  this->duration = duration;
}
