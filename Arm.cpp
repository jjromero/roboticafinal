#include "Arm.h"

Arm::Arm(int servoBottom, int servoTop, int servoTool, int coilA, int coilB, int coilC, int coilD, int duration)
    : stepper(coilA, coilB, coilC, coilD, duration),
      servos(servoBottom, servoTop, servoTool, duration, 5)
{
}

void Arm::begin(Angles angles)
{
  servos.begin(angles.b, angles.c, 0);
  stepper.begin();
}

void Arm::setAngles(Angles angles)
{
  stepper.setAngle(angles.a);
  servos.setAngles(angles.b, angles.c);
}

void Arm::update()
{
  servos.update();
  stepper.update();
}

bool Arm::isMoving()
{
  return stepper.isMoving() && servos.isMoving();
}

void Arm::setDuration(int duration)
{
  servos.setDuration(duration);
  stepper.setDuration(duration);
}
