#include "Joystick.h"

Joystick::Joystick(int PIN_X, int PIN_Y)
{
  this->PIN_X = PIN_X;
  this->PIN_Y = PIN_Y;
}

JoystickData Joystick::read()
{
  JoystickData data;
  data.X = analogRead(this->PIN_X) - 512;
  data.Y = analogRead(this->PIN_Y) - 512;
  return data;
}
